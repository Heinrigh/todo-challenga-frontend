# todo-challenge-frontend
This repository contains the frontend implementation of the Todo Challenge application.
## Project setup
Before running the application, make sure you have Node.js and npm installed on your system.
1. ### Install Dependencies:
    * Run the following command to install project dependencies:
    ```
    npm install
    ```

### development Mode
To run the application in development mode with hot-reloads, use the following command:
```
npm run serve
```
This will start a development server, and you can access the application at `http://localhost:8081` .

### Production Build
To compile and minify the application for production, run:

```
npm run build
```
This will generate a production build of the application in the dist directory.


## Deployment
### Local Deployment
* To deploy the application locally, you need to specify the backend API URL in the http-common.js and the auth.service.js files.

* After configuring the backend URL, run the following command to start the application:

```
npm run serve
```
Access the application in your browser at `http://localhost:8081` .

### Docker Deployment
To deploy the application using Docker, follow these steps:

1. Build Docker Image
   * Build a Docker image for the application by running the following command:
   
    ```
    docker build -t todo-challenge-frontend .
    ```
2. Run Docker Container:
   * After the Docker image is built, run the following command to start a Docker container:

    ```
    docker run -d -p 8081:80 todo-challenge-frontend
    ```
   Access the application in your browser at `http://<your_server_ip>:8081`.